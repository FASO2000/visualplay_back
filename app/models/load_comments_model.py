from app.database.bd_conection import BaseDatos

bd = BaseDatos()

class load_comments_model:

    def load_comments(self, id_video):
        try:
            sql = (f"SELECT (SELECT name FROM vp_users WHERE c.user GROUP BY id LIMIT 1) AS user, c.id_video, c.comments FROM vp_comments c WHERE c.id_video = {id_video};")
            result = bd.consultar(sql)
            return result
        except Exception as e:
            print("error in method load_comments argument:", e.args[0])
            return False