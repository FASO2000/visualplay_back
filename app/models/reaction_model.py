from app.database.bd_conection import BaseDatos

bd = BaseDatos()


class reaction_model:
    #REGISTRAR 
    def reaction(self, id_video, user):
        try:
            res = bd.insert(
                f"INSERT INTO vp_reactions (id_video, user) VALUES ({id_video}, {user})")
            return res
        except Exception as e:
            print("error in method reaction argument:", e.args[0])
            return False

    #DAR LIKE AL VIDEO
    def like(self,id_video, like):
        
        try:
            res = bd.update(
                f"UPDATE vp_videos v SET v.like = IF({like} = 1, (v.like - 1), (v.like + 1)) WHERE v.id = {id_video}")
            return res
        except Exception as e:
            print("error in method like argument:", e.args[0])
            return False

    #QUITAR LIKE AL VIDEO
    def nolike(self,id_video,user):
        try:
            res = bd.consultar(
                f"SELECT * FROM vp_reactions WHERE id_video = {id_video} AND user = {user};")
            
            if res:
                return True
            else:
                return False

        except Exception as e:
            print("error in method nolike argument:", e.args[0])
            return False
