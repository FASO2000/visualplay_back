from app.database.bd_conection import BaseDatos

bd = BaseDatos()


class home_model:

    def menus_by_rol(self, rol):
        try:
            sql = (f"SELECT name_menu FROM vp_menus WHERE rol_id = {rol}")
            result = bd.consultar(sql)
            return result
        except Exception as e:
            print("error in method menus_by_rol argument:", e.args[0])
            return False
    
    def get_all_videos(self):
        try:
            sql = (f"SELECT * FROM vp_videos")
            result = bd.consultar(sql)
            return result
        except Exception as e:
            print("error in method menus_by_rol argument:", e.args[0])
            return False
