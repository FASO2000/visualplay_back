import base64
from botocore.exceptions import ClientError
from app.database.bd_conection import BaseDatos

bd = BaseDatos()

class upload_video_model:
     
    def upload_file(self,file,clien_s3,bucket_name,nameFile,mime):
        try:
            #Decodifico el archivo que me llega en base64            
            data = base64.b64decode(file)

            #Subo el archivo en el s3 pasandole los siguientes parametros            
            result = clien_s3.put_object(Body=data,Bucket=bucket_name,Key=nameFile,ContentType=mime)
            return result
        except ClientError as e:
            print("Unexpected error: %s" % e)   
        except Exception as e:
            print("error in method upload_video argument:", e.args[0])
            return False

    def insert_video(self,name,path_video,title,description,path_miniature,file_b64,miniature_b64,mime_min,mime_video):
        try:
            sql = (f"INSERT INTO vp_videos(name,path_video,path_miniature,title,description,video_convert,image_convert,mime_image,mime_video) values \
            ('{name}','{path_video}','{path_miniature}','{title}','{description}','{file_b64}','{miniature_b64}','{mime_min}','{mime_video}')")
            result = bd.insert(sql)
            return result
        except Exception as e:
            print("error in method upload_video argument:", e.args[0])
            return False