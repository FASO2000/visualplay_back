import re
import hashlib
import boto3
from credenciales_aws import *
from app.helpers.response import response
from app.models.login_model import loginModel

modelLogin = loginModel()

class loginClass:
    
    LOGIN = 1
    REFRESH_TOKEN = 2

    def __init__(self, client_id=""):

        #Creo el cliente con el servicio de cognito
        self.client = boto3.client(
            'cognito-idp',
            aws_access_key_id=ACCESS_KEY,
            aws_secret_access_key=ACCESS_SECRET,
            region_name = REGION
        )
        self.client_id = client_id   

    def login(self, email,passw,action):
        
        #Convierto el string de la contraseña en un hash
        hash_object = hashlib.md5(passw.encode())
        password = hash_object.hexdigest()
        
        #Parametros de validacion de usuario
        auth_params = {
            "USERNAME": email,
            "PASSWORD": passw
        }

        SpecialSym = ['/','\&']
        #Validar campos vacios
        if email == '':
            return response(400, None, 'Favor digitar el email', 'error')
        elif password == '':
            return response(400, None, 'Favor digitar la contraseña', 'error')
        
        #Validar espacios en blanco
        email_space = email.isspace()
        pass_space = password.isspace()
        if email_space and not email_space.isspace():
            return response(400, None, 'El email no debe contener espacios', 'error')
        elif pass_space and not pass_space.isspace():
            return response(400, None, 'La contraseña no debe contener espacios', 'error')
        
        #Valido comillas simples
        mail = email.replace("'",'"')
        passw = password.replace("'",'"')

        #Valido caracteres espeiales en el correo
        correo_valido = self.es_correo_valido(mail)        
        if correo_valido == True:
            #Valido caracteres espeiales en la contraseña
            if not any(char in SpecialSym for char in passw):          
                #Valido en la base de datos que coincida
                result = modelLogin.get_email_and_password(mail,passw)                    
                if result:
                    #Valido usuario existente en cognito
                    existe_user = self.validation_cognito(auth_params,action)
                    return response(200, existe_user, 'logeado correctamente', 'success')
                else:
                    return response(400, None, 'Email o contraseña incorrecta', 'error')
            else:                
                return response(400, None, 'Error de caracteres no permitidos en la contraseña', 'error')


        else:
            return response(400, None, 'Error de caracteres no permitidos en el correo', 'error')

    def es_correo_valido(self,correo):
        expresion_regular = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
        return re.match(expresion_regular, correo) is not None

    def validation_cognito(self,auth_params,action):

        auth_flow = self.get_auth_flow(action)
        
        response = self.client.initiate_auth(
            ClientId=self.client_id,
            AuthFlow=auth_flow,
            AuthParameters=auth_params
        )
        
        return response
    
    def get_auth_flow(self, action=0):

        if action == self.LOGIN:
            auth_flow = "USER_PASSWORD_AUTH"
        elif action == self.REFRESH_TOKEN:
            auth_flow = "REFRESH_TOKEN"
        else:
            auth_flow = ""

        return auth_flow