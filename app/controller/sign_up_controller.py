import re
import hashlib
import boto3
from botocore.exceptions import ClientError
from app.helpers.response import *
from credenciales_aws import *
from app.models.sign_up_model import create_user_model 

createusr_modl = create_user_model()

class sign_up:

    def __init__(self, client_id=""):
        
        #Creo el cliente con el servicio de cognito
        self.client = boto3.client(
            'cognito-idp',
            aws_access_key_id=ACCESS_KEY,
            aws_secret_access_key=ACCESS_SECRET,
            region_name = REGION
        )
        self.client_id = client_id


    def signUp(self,email,passw):
        
        try:
            #Obtengo el usuario y contraseña
            username = email
            password = passw   

            #Hago la peticion de el envio del codigo de confirmacion al correo
            info = self.client.sign_up(
                ClientId=self.client_id,
                Username=username,
                Password=password,
                UserAttributes=[
                    {
                        'Name': 'email',
                        'Value': email
                    }
                ]
            )

            return response(200, info, 'Create user correctly', 'success')
            
        except ClientError as e:
            if e.response['Error']['Code'] == 'UsernameExistsException':
                return response(400,None,'Este correo ya se encuentra registrado','error')
            else:
                return response(400,None,e.response,'error')
        except Exception as e:
            print("error in method signUp argument:", e.args[0])
            return response(400,None,e.args[0],"exception")
            

    def resend_confirmation_code(self,email):
        
        try:
            #Obtengo el usuario
            username = email

            #Hago la peticion de el reenvio del codigo al correo
            result = self.client.resend_confirmation_code(
                ClientId=self.client_id,
                Username=username
            )

            #Valido la respuesta para enviar un mensaje
            if result['ResponseMetadata']['HTTPStatusCode'] == 200:
                return response(200,'Código reenviado a el correo ' + email , 'Send code correctly', 'success')
            else:
                return response(400,None,'Ha ocurrido un error al reenviar el código',"error")
            
        except Exception as e:
            print("error in method resend_confirmation_code argument:", e.args[0])
            return response(400, None, e.args[0], 'exception')

    def confirm_sign_up(self,email,code):
        try:

            #Obtengo el mail y el codigo de confirmacion que llego al correo        
            username = email
            codeConfirmation = code

            #Hago la peticion de la confirmacion del codigo
            result = self.client.confirm_sign_up(
                ClientId=self.client_id,
                Username=username,
                ConfirmationCode=codeConfirmation
            )

            #Valido la respuesta para enviar un mensaje
            if result['ResponseMetadata']['HTTPStatusCode'] == 200:
                return response(200,'Correo electrónico confirmado con éxito!', 'Send code correctly', 'success')
            else:
                return response(400,None,'Ha ocurrido un error al confirmar el correo',"error")
                

        except Exception as e:
            print("error in method confirm_sign_up argument:", e.args[0])
            return response(400, None, e.args[0], 'exception')

    def validatePassword(self,password):
        try:
            
            if len(password) < 6:
                return "La contraseña debe ser mayor a 6 dígitos"
            elif re.search('[0-9]',password) is None:
                return "La contraseña debe tener por lo menos un numero"
            elif re.search('[A-Z]',password) is None: 
                return "La contraseña debe tener por lo menos una letra mayúscula"
            elif re.search('[a-z]',password) is None: 
                return "La contraseña debe tener por lo menos una letra minúscula"
            else:
                return True

        except Exception as e:
            print("error in method validatePassword argument:", e.args[0])
            return response(400, None, e.args[0], 'exception')

    def guardar_usuario(self,email,password,nombre,username):
        
        try:
            #Convierto el string de la contraseña en un hash
            hash_object = hashlib.md5(password.encode())            
            password = hash_object.hexdigest()

            #Valido comillas simples
            email = email.replace("'",'')
            password = password.replace("'",'')
            nombre = nombre.replace("'",'')
            username = username.replace("'",'')

            result =  createusr_modl.save_users(email,password,nombre,username) 
            if result:
                return response(200,result, 'Saved user correctly', 'success')
            else:
                return response(400,None, 'error save user', 'error')
            
        except Exception as e:
            print("error in method guardar_usuario argument:", e.args[0])
            return response(400, None, e.args[0], 'exception')

    def es_correo_valido(self,correo):
        expresion_regular = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
        return re.match(expresion_regular, correo) is not None