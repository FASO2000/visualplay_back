from app.helpers.response import response
from app.models.load_comments_model import load_comments_model

load_com_mdl = load_comments_model()

class load_comments_controller:

    def load_comments(self, id_video):
        try:
            list_comments = []
            comments = load_com_mdl.load_comments(id_video)
            if not comments:
                return response(400, comments, 'Comments not found', 'error')

            for item in comments:

                comments = {
                    "user": item['user'],
                    "id_video": item['id_video'],
                    "comments": item['comments']
                } 

                list_comments.append(comments)


            return response(200, list_comments, 'Comments loaded correctly', 'success')

        except Exception as e:
            print(e)
            print("error in method load_video argument:", e.args[0])
            return response(400, None, e.args[0], 'Exception')
        
