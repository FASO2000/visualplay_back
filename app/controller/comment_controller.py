from app.helpers.response import response
from app.models.comment_model import save_comment_model

save_comments_modl = save_comment_model()
class comment_controller:

    def guardar_comentario(self,id_video, comments, user):
        
        try:

            result = save_comments_modl.save_comments(id_video, comments, user)

            if result:
                return response(200,result, 'Saved comments correctly', 'success')
            else:
                return response(400,None, 'error saving comments', 'error')
            
        except Exception as e:
            print("error in method comment argument:", e.args[0])
            return response(400, None, e.args[0], 'exception')




