import base64
import boto3
import json
import codecs
from app.helpers.response import response
from app.models.load_video_model import load_video_model
from credenciales_aws import *

load_mdl = load_video_model()


class load_video_controller:
    def load_video(self, id_video):
        try:

            info_video = load_mdl.load_video(id_video)
            if not info_video:
                return response(400, info_video, 'Video not found', 'error')

            title = [l['title'] for l in info_video][0]
            description = [l['description'] for l in info_video][0]
            video = [l['video_convert'] for l in info_video][0]
            mime_video = [l['mime_video'] for l in info_video][0]
            
            info_video = {
                "id": id_video,
                "title": title,
                "description": description,
                "mime_video": str(mime_video).replace("b'","").replace("'",""),
                "video": str(video).replace("b'","").replace("'","")
            }

            return response(200, info_video, 'Video loaded correctly', 'success')

        except Exception as e:
            print(e)
            print("error in method load_video argument:", e.args[0])
            return response(400, None, e.args[0], 'Exception')
