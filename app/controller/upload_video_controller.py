import base64
from email.mime import base
import boto3
from app.helpers.response import response
from app.models.upload_video_model import upload_video_model
from credenciales_aws import *

upload_mdl = upload_video_model()

class upload_video_controller:
    def upload_video(self, file, name, mime,mime_min,title,description,miniature):
        try:

            # Obtener ruta en el s3
            routeFile = "videos/"
            route_file_min = "miniaturas/"
            nameFile = f"{routeFile}{name}"
            name_file_min = f"{route_file_min}{name}"
            
            # Subir el video a s3
            upload_mdl.upload_file(
                file,   CLIENT_S3, BUCKET_NAME, nameFile, mime)
            
            # Subir la imagen miniatura a s3
            upload_mdl.upload_file(
                miniature, CLIENT_S3, BUCKET_NAME, name_file_min, mime_min)

            # Guardamos los datos adicionales del video    
            result = upload_mdl.insert_video(name, nameFile,title,description,name_file_min,file,miniature,mime_min,mime)
            if result:
                return response(200, result, 'Video loaded correctly', 'success')
            else:
                return response(400, None, 'Error to charge video', 'error')

        except Exception as e:
            print("error in method upload_video argument:", e.args[0])
            return response(400, None, e.args[0], 'Exception')
