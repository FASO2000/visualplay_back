import base64
import json
from urllib import request
from credenciales_aws import *
from app.helpers.response import response
from app.models.home_model import home_model

home_mdl = home_model()


class home_controller:

    def menus_by_rol(self, rol):
        try:
            menus = home_mdl.menus_by_rol(rol)
            if menus:
                return response(200, menus, 'Menus loaded correctly', 'success')
            else:
                return response(400, None, 'Error loading menus', 'error')
        except Exception as e:
            print("error in method menus_by_rol argument:", e.args[0])
            return response(400, None, e.args[0], 'Exception')

    def view_videos_principal(self):
        try:
            list_videos = []

            videos = home_mdl.get_all_videos()    
            if not videos:
                return response(400, None, 'Error loading videos', 'error')
                
            for item in videos:
                info_video = {
                    'id':item['id'],
                    'name':item['name'],
                    'title':item['title'],
                    'mime_image':item['mime_image'],
                    'mime_video':item['mime_video'],
                    'miniature':str(item['image_convert']).replace("b'","").replace("'",""),
                    'video_convert':str(item['video_convert']).replace("b'","").replace("'","")
                }
                list_videos.append(info_video)
            return response(200, list_videos, 'Menus loaded correctly', 'success')

        except Exception as e:
            print("error in method view_videos_principal argument:", e.args[0])
            return response(400, None, e.args[0], 'Exception')
