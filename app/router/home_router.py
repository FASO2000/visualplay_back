
import json
from app.controller.home_controller import home_controller
from app.helpers.response import result_response

home_contrll = home_controller()


def menus_by_rol(event, context):
    data = json.loads(event['body'])
    rol = data['rol']
    result = home_contrll.menus_by_rol(rol)
    return result_response(result)


def view_videos_principal(event, context):
    result = home_contrll.view_videos_principal()
    return result_response(result)
