import json
import os
from credenciales_aws import *
from app.helpers.response import *
from app.controller.sign_up_controller import sign_up 

create_ctrll = sign_up()

def create_user(event, context):
    
    #Obtengo los parametros de usuario, contraseña y clienteId del grupo de usuario en cognito
    data = json.loads(event['body'])
    email = data.get('email', '')
    password = data.get('password', '')
    nombre = data.get('nombre', '')
    username =  data.get('username', '')
    
    #Valido que la contraseña cumpla con los caracteres de
    password_success = create_ctrll.validatePassword(password)
    if password_success != True:
        return response(400,None,password_success,'error')

    #Valido el gmail
    val_gmail = create_ctrll.es_correo_valido(email)

    if val_gmail != True:
        return response(400,None,'Error de caracteres no permitidos en el correo','error')

    client_id = ID_CLIENTE

    #Envio el clienteId y llamo a la clase para crear el usuario
    cognito = sign_up(client_id)
    result = cognito.signUp(email,password)
    print(result)
    #Guardo los datos de inicio de sesión
    if result['data']['ResponseMetadata']['HTTPStatusCode'] == 200:
        respuesta = create_ctrll.guardar_usuario(email,password,nombre,username)
    else:
        return response(400,None,'Ha ocurrido un error al guardar el usuario','error')

    return result_response(respuesta)

def confir_user(event, context):    

    #Obtengo los parametros de usuario, contraseña y clienteId del grupo de usuario en cognito
    data = json.loads(event['body'])
    email = data.get('email', '')
    code = data.get('code', '')
    print(email)
    print(email)

    #Obtengo el id del cliente del grupo de usuario de cognito 
    client_id = ID_CLIENTE

    #Envio el clienteId y llamo a la clase para crear el usuario
    cognito = sign_up(client_id)
    result = cognito.confirm_sign_up(email,code)

    return result_response(result)

def resend_code(event, context):

    #Obtengo el parametro de email y clienteId del grupo de usuario en cognito
    data = json.loads(event['body'])
    email = data.get('email', '')
    client_id = ID_CLIENTE

    #Envio el clienteId y llamo a la clase para reenviar el codigo de confirmacion
    cognito = sign_up(client_id)
    result = cognito.resend_confirmation_code(email)

    return result_response(result)