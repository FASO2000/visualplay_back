import json
from app.helpers.response import result_response
from app.controller.load_video_controller import load_video_controller

load_contrll = load_video_controller()

def load_video(event, context):
    data = json.loads(event['body'])
    id_video = data.get('id_video','')

    result = load_contrll.load_video(id_video)
    return result_response(result)