import json
from app.helpers.response import result_response
from app.controller.comment_controller import comment_controller

comment_contrll = comment_controller()

def comment (event, context):
    data = json.loads(event['body'])
    id_video = data.get('id_video','')
    comments = data.get('comments','')
    user = data.get('user','')

    result = comment_contrll.guardar_comentario(id_video, comments, user)
    return result_response(result)