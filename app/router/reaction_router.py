import json
from app.helpers.response import result_response
from app.controller.reaction_controller import reaction_controller

comment_contrll = reaction_controller()

def reaction (event, conntext):
    data = json.loads(event['body'])
    id_video = data.get('id_video','')
    user = data.get('user','')
    like = data.get('like','')

    result = comment_contrll.reaccionar(id_video, user, like)
    return result_response(result)
