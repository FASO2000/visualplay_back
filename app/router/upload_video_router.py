import json
from app.helpers.response import result_response
from app.controller.upload_video_controller import upload_video_controller

video_contrll = upload_video_controller()

def upload_video(event, context):
    data = json.loads(event['body'])
    file = data.get('file','')
    name = data.get('name','')
    mime = data.get('mime','')
    mime_min = data.get('mime_min','')
    title = data.get('title','')
    miniature = data.get('miniature','')
    description = data.get('description','')

    result = video_contrll.upload_video(file,name,mime,mime_min,title,description,miniature)
    return result_response(result)