import json
from app.helpers.response import result_response
from app.controller.load_comments_controller import load_comments_controller

load_comments_contrll = load_comments_controller()

def load_comments(event, context):
    data = json.loads(event['body'])
    id_video = data.get('id_video','')

    result = load_comments_contrll.load_comments(id_video)
    return result_response(result)