import json
import os
from credenciales_aws import *
from app.helpers.response import result_response
from app.controller.login_controller import loginClass

clsLogin = loginClass()

def login(event, context):    

    #Obtengo los parametros de usuario y contraseña y el cliente id del cognito
    data = json.loads(event['body'])
    email = data.get('email', '')
    password = data.get('password', '')
    action = data.get('action', '')

    #Obtengo el id del cliente del cognito
    client_id = ID_CLIENTE

    loginClient = loginClass(client_id)
    result = loginClient.login(email,password,action)    
    return result_response(result)    