import pymysql

class BaseDatos:

    def conexion(self):    
        try:
            connection = False
            connection = pymysql.connect(
                user='root'     ,
                password='',
                host='localhost',                      
                port=3306,
                database='visualplay',
                charset='utf8mb4',
                cursorclass=pymysql.cursors.DictCursor
            )

            return connection

        except pymysql.Error as e:
            return False
    
    def consultar(self, sql):
        connection = self.conexion()
        if connection == False:
            return False

        result = False
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql) 
                result = cursor.fetchall()
        finally:
            connection.close()

        return result

    def insert(self, sql):
        connection = self.conexion()
        if connection == False:
            return False

        result = False
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql)
                n=cursor.rowcount
                connection.commit()  
                result = n
        finally:
            connection.close()

        return result
    
    def delete(self, sql):
        
        connection = self.conexion()
        if connection == False:
            return False        

        result = False
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql) 
                connection.commit()  
                result = True
        finally:
            connection.close()

        return result
    
    def update(self, sql):
        
        connection = self.conexion()
        if connection == False:
            return False       

        result = False
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql) 
                connection.commit() 
                result = True
        finally:
            connection.close()

        return result
    
    
    def insert_masive(self, sql, data):
        connection = self.conexion()
        if connection == False:
            return False        
            
        result = False
        try:
            with connection.cursor() as cursor:
                cursor.executemany(sql, data)
                n=cursor.rowcount
                connection.commit()  
                result = n
        finally:
            connection.close()

        return result