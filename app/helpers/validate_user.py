import re
import boto3
from botocore.exceptions import ClientError
from app.helpers.response import response


class validate_user:
    
    def validatePassword(self,password):
        try:
            
            if len(password) < 6:
                return "La contraseña debe ser mayor a 6 dígitos"
            elif re.search('[0-9]',password) is None:
                return "La contraseña debe tener por lo menos un numero"
            elif re.search('[A-Z]',password) is None: 
                return "La contraseña debe tener por lo menos una letra mayúscula"
            elif re.search('[a-z]',password) is None: 
                return "La contraseña debe tener por lo menos una letra minúscula"
            else:
                return True

        except Exception as e:
            print("error in method validatePassword argument:", e.args[0])
            return response(400, None, e.args[0], 'Exception')


    def es_correo_valido(self,correo):
        expresion_regular = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
        return re.match(expresion_regular, correo) is not None