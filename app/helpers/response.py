import json
import datetime


def response(status_code, data, messagge, status):
    return {
        "status_code": status_code,
        "data": data,
        "messagge": messagge,
        "status": status
    }


def result_response(result):

    str_result = str(result)
    if "exception" in str_result:
        return response(400, None, "Error interno del sistema", "exception")

    if result["status"] == "success":
        return response(200, result["data"], result["messagge"], result["status"])
    elif result["status"] == "error":
        return response(400, None, result["messagge"], result["status"])
